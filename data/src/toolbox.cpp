#pragma once
#include "toolbox.h"

Tool_One::Tool_One(std::size_t compteur_max=32){
  max_iteration = compteur_max;
}

Tool_One::~Tool_One(){

}

void Tool_One::SuperFunction(){
  for(std::size_t i = 0; i < max_iteration; ++i){
    std::cout << "Le compteur vaut : " << i << std::endl;
  }
}
