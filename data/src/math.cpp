#include "math.h"

Point2D::Point2D(float _x, float _y) :
  position_x(_x),
  position_y(_y)
{}

Point2D::~Point2D(){}

void Point2D::setPosition(float _homogene){
  setPosition(_homogene, _homogene);
}

void Point2D::setPosition(float _x, float _y){
  position_x = _x;
  position_y = _y;
}

void Point2D::setPosition(Point2D _point){
  setPosition(_point.getX(), _point.getY());
}

float Point2D::getX(){
  return position_x;
}

float Point2D::getY(){
  return position_y;
}
